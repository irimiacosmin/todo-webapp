window.onload = getAndPopulateToDoList;
let SERVER_URL = 'http://localhost:3000/todo/';

let priorities = {
    "0": {
        'priority': 'No priority',
        'class': 'badge-success'
    },
    "1": {
        'priority': 'Low',
        'class': 'badge-info'
    },
    "2": {
        'priority': 'Medium',
        'class': 'badge-warning'
    },
    "3": {
        'priority': 'Urgent',
        'class': 'badge-danger'
    },
    "-1": {
        'priority': 'Completed',
        'class': 'badge-primary'
    }
}

function getPriority(id) {
    let priority = priorities[id];
    if (priority === undefined) {
        return {
            'priority': 'Unknown',
            'class': 'badge-secondary'
        }
    }
    return priority;
}

function getPriorityId(prClass) {
    let prElement = Object.keys(priorities).filter(key => priorities[key]['class'] === prClass)
    return prElement.length === 1 ? prElement[0] : undefined;
}
const getAllToDoS = async (type) => {
    return (await fetch(SERVER_URL, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'username': 'cosmin'
        }
    })).json().then(response => {
        if (response !== undefined) {
            populateToDoList(response['todoList'], type)
        }
    });
}

const updateToDo = async (toDo) => {
    return (await fetch(SERVER_URL + toDo['id'], {
        method: 'PUT',
        body: JSON.stringify(toDo),
        headers: {
            'Content-Type': 'application/json',
            'username': 'cosmin'
        }
    })).json().then(response => {
        if (response !== undefined) {
            populateToDoList(response['todoList'], 'all')
        }
    });
}

const createToDo = async (toDo) => {
    return (await fetch(SERVER_URL, {
        method: 'POST',
        body: JSON.stringify(toDo),
        headers: {
            'Content-Type': 'application/json',
            'username': 'cosmin'
        }
    })).json().then(response => {
        if (response !== undefined) {
            populateToDoList(response['todoList'], 'all')
        }
    });
}

function getAndPopulateToDoList() {
    getAllToDoS('all');
}

function populateOnlyWithActive() {
    getAllToDoS('active');
}

function populateOnlyWithFinished() {
    getAllToDoS('finished');
}

function populateToDoList(elementList, type) {
    let tableParent = document.getElementById("dataTableBody");
    tableParent.innerHTML = null;
    elementList.filter(x => {
        switch (type) {
            case 'active': {
                return !x['checked'];
            }
            case 'finished': {
                return x['checked'];
            }
            default: {
                return x;
            }
        }
    }).sort((a, b) => b['id'] - a['id'])
        .forEach(toDo => {
            let tableChild = createItemDivBy(toDo);
            tableParent.appendChild(tableChild)
        })
}

function toDoCheckedChanged(event) {
    let changedChecked = event.target.className.toString().includes('-outline-');
    let progressLevel = changedChecked ? 10 : 0;
    let priorityLevel = changedChecked ? -1 : 0;
    let elementId = parseInt(event.target.id.split('_')[1]);
    let newToDo = {
        'id': elementId,
        'checked': changedChecked,
        'progressLevel' : progressLevel,
        'priorityLevel' : priorityLevel
    };
    updateToDo(newToDo).then(result => console.log(result))
}

function toDoTextChanged(event) {
    let changedText = event.target.value.toString();
    let elementDistinctSigns = event.target.id.split('_');
    let elementId = parseInt(elementDistinctSigns[1]);

    var date = new Date;
    let finishTime = date.getHours() + ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
    if (elementId === 0) {
        createToDo({
            'id': elementId,
            'text': changedText,
            'checked': false,
            'finishTime': finishTime,
            "priorityLevel": 0,
            "progressLevel": 0
        });
    } else {
        let newToDo = {'id': elementId}
        switch (elementDistinctSigns[0]) {
            case 'label' : {
                newToDo['text'] = changedText
                break;
            }
            case 'time' : {
                if (!(/^(([01])[0-9]|2[0-3]):([0-5][0-9])$/.test(changedText))) {
                    showAlertForTwoSeconds(event.target, 'Time input is invalid')
                    return;
                }
                newToDo['finishTime'] = changedText
                break;
            }
            case 'progress' : {
                let pLevel = parseInt(changedText)
                newToDo['progressLevel'] = pLevel
                newToDo['checked'] = pLevel === 10;

                let currentSelectedElement = document.getElementById('badge-main_' + elementDistinctSigns[1] + '_' + elementDistinctSigns[2]).className.split(' ')
                if (currentSelectedElement.length === 4) {
                    let curentPriority = getPriorityId(currentSelectedElement[3]);
                    if (curentPriority !== undefined) {
                        let currentPriorityParsed = parseInt(curentPriority);
                        console.log(currentPriorityParsed)
                        if (currentPriorityParsed === -1 && pLevel !== 10) {
                            newToDo['priorityLevel'] = 0;
                        } else if (currentPriorityParsed !== -1 && pLevel === 10) {
                            newToDo['priorityLevel'] = -1;
                        }
                    }
                } else {
                    newToDo['priorityLevel'] = pLevel === 10 ? -1 : 0;
                }
                break;
            }
        }
        updateToDo(newToDo);
    }
}

function getIdentifier(type, toDoElement) {
    return type + '_' + toDoElement['id'] + '_' + toDoElement['text'].replace(' ', '');
}

function addNewToDoItem() {
    let tableParent = document.getElementById("dataTableBody");
    var date = new Date;
    let finishTime = date.getHours() + ':' + date.getMinutes();
    let newChild = createItemDivBy({
        'id': 0,
        'text': '',
        'checked': false,
        'finishTime': finishTime,
        "priorityLevel": 0,
        "progressLevel": 0
    }, true);
    tableParent.insertBefore(newChild, tableParent.firstChild);
}

function dropdownItemOnClick(event) {
    let elementDistinctSigns = event.target.id.split('_');
    let elementId = parseInt(elementDistinctSigns[1]);
    let priorityLevel = getPriorityId(elementDistinctSigns[0])
    let currentSelectedElement = document.getElementById('badge-main_' + elementDistinctSigns[1] + '_' + elementDistinctSigns[2]).className.split(' ')
    if (currentSelectedElement.length !== 4) {
        return;
    }
    let curentPriority = getPriorityId(currentSelectedElement[3]);
    if (priorityLevel === undefined || curentPriority === undefined) {
        return;
    }
    let parsedPriorityLevel = parseInt(priorityLevel);
    curentPriority = parseInt(curentPriority);
    let checkedStatus = parsedPriorityLevel === -1;
    let newToDo = {
        'id': elementId,
        'priorityLevel' : parsedPriorityLevel,
        'checked' : checkedStatus
    };
    if (curentPriority === -1 && parsedPriorityLevel !== -1) {
        newToDo['progressLevel'] = 0;
    } else if(curentPriority !== -1 && parsedPriorityLevel === -1){
        newToDo['progressLevel'] = 10;
    }
    updateToDo(newToDo);
}

function getAllBadgesDropdown(toDoElement) {
    let badgeDetails = getPriority(toDoElement['priorityLevel']);
    if (badgeDetails === undefined) {
        return undefined;
    }
    let outerDiv = document.createElement('div');
    outerDiv.className = 'dropdown no-arrow show';

    let selectedPriority = document.createElement('span');
    selectedPriority.className = 'dropdown-toggle badge badge-pill ' + badgeDetails['class'];
    selectedPriority.textContent = badgeDetails['priority'];
    selectedPriority.id = getIdentifier('badge-main', toDoElement)
    selectedPriority.setAttribute('data-toggle', 'dropdown');

    let optionsOfPriority = document.createElement('div');
    optionsOfPriority.className = 'dropdown-menu';
    optionsOfPriority.setAttribute('aria-labelledby', 'dropdownMenuLink');

    for(let i=-1; i<4; i++) {
        let optionZeroDiv = document.createElement('span');
        let optionZero = getPriority(i.toString());
        optionZeroDiv.className = 'dropdown-item badge badge-pill ' + optionZero['class']
        optionZeroDiv.id = getIdentifier(optionZero['class'], toDoElement)
        optionZeroDiv.textContent = optionZero['priority'];
        optionZeroDiv.onclick = dropdownItemOnClick;
        optionsOfPriority.appendChild(optionZeroDiv);
    }

    outerDiv.appendChild(selectedPriority);
    outerDiv.appendChild(optionsOfPriority);
    return outerDiv;
}

function getCheckBoxButtonClassFrom(checkedProp, isDisabled = false) {
    if (isDisabled) {
        return {
            'class': 'tableBtn btn btn-outline-danger disabled',
            'name': 'N/A'
        };
    }
    if (checkedProp) {
        return {
            'class': 'tableBtn btn btn-success',
            'name': 'Uncheck'
        };
    } else {
        return {
            'class': 'tableBtn btn btn-outline-success',
            'name': 'Check'
        };
    }
}

function createItemDivBy(toDoElement, checkBoxDisabled = false, textBoxDisabled = false) {
    let trElement = document.createElement('tr');
    let doneElement = document.createElement('th');

    let inputElement = document.createElement('button');
    let checkBoxIdentifier = getIdentifier('checkbox', toDoElement);
    inputElement.id = checkBoxIdentifier;
    inputElement.name = checkBoxIdentifier;
    inputElement.type = 'button';
    let checkBoxButtonClassFrom = getCheckBoxButtonClassFrom(toDoElement['checked'], checkBoxDisabled);
    inputElement.className = checkBoxButtonClassFrom['class']
    inputElement.textContent = checkBoxButtonClassFrom['name'];
    inputElement.tagName = toDoElement['checked'];
    inputElement.onclick = toDoCheckedChanged;
    doneElement.className = 'doneTh';
    doneElement.appendChild(inputElement);

    let nameElement = document.createElement('th');
    let labelElement = document.createElement('input');
    let labelIdentifier = getIdentifier('label', toDoElement);
    labelElement.id = labelIdentifier;
    labelElement.name = labelIdentifier;
    labelElement.className = 'unborderedInput oneHundredPercent';
    labelElement.value = toDoElement['text'];
    labelElement.onchange = toDoTextChanged;
    nameElement.appendChild(labelElement);

    let progressElement = document.createElement('th');
    let rangeProgressElement = document.createElement('input');
    rangeProgressElement.type = 'range';
    rangeProgressElement.id = getIdentifier('progress', toDoElement)
    rangeProgressElement.className = 'progressRange';
    rangeProgressElement.min = '1';
    rangeProgressElement.max = '10';
    rangeProgressElement.value = toDoElement['progressLevel'];
    rangeProgressElement.onchange = toDoTextChanged
    progressElement.className = 'progressTh';
    progressElement.appendChild(rangeProgressElement);

    let levelElement = document.createElement('th');
    levelElement.className = 'levelTh';
    levelElement.appendChild(getAllBadgesDropdown(toDoElement));

    let targetTimeElement = document.createElement('th');
    let targetInputElement = document.createElement('input');
    let identifier = getIdentifier('time', toDoElement);
    targetTimeElement.id = 'target_' + identifier;
    targetInputElement.id = identifier;
    targetInputElement.name = identifier;
    targetInputElement.value = toDoElement['finishTime'];
    targetInputElement.className = 'unborderedInput oneHundredPxClass';
    targetInputElement.onchange = toDoTextChanged;
    targetTimeElement.className = 'timeTh';
    targetTimeElement.appendChild(targetInputElement);

    trElement.appendChild(doneElement);
    trElement.appendChild(nameElement);
    trElement.appendChild(progressElement);
    trElement.appendChild(levelElement);
    trElement.appendChild(targetTimeElement);
    return trElement;
}

function showAlertForTwoSeconds(element, text) {
    let parent = document.getElementById('target_' + element.id);
    element.className = 'invalidInput oneHundredPxClass';
    parent.innerHTML = null;
    parent.appendChild(element);
}

function getAlert(text) {
    let alert = document.createElement('div');
    alert.className = 'alert alert-danger';
    alert.role = 'alert';
    alert.textContent = text;
    return alert;
}